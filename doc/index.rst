:program:`mplpub`
*****************

`mplpub` is a simple python module that provides a number of templates for publication quality `matplotlib` figures.
It can be installed using `pip` as follows::

  python3 -m pip mplpub

For maximum plot quality and flexibility when including mathematical equations, :program:`mplpub` uses your system's LaTeX distribution to render labels etc.
Thus you will need a working LaTeX distribution, and the `helvet` LaTeX package which is used to render all text and math using Helvetica.
This package in typically shipped with comprehensive LaTeX distributions, e.g., TeXLive.

Templates
=========

Currently the following templates are available:

  * `acs`
  * `acs_dcol`
  * `base`
  * `base_2.0_update`
  * `jpcl_toc`
  * `natcom`
  * `natcom_dcol`
  * `phd`
  * `phd_fw`
  * `pr`
  * `rsc`
  * `rsc_dcol`

Upper-case greek letters
========================

Upper-case greek letters are generally not available when using sans-family fonts such as Helvetica.
In this case, one can often get acceptable results by falling back to a serif font.
In :program:`mplpub` this has been conveniently implemented as a LaTeX command called ``\UG`` that can be invoked in you LaTeX-containing python strings like so::

  r"Upper-case greek letters: $\UG{\Delta}$"

Commands
========

.. automodule:: mplpub
   :members:
   :undoc-members:
   :inherited-members:

Examples
========

* A simple plot showcasing the basic features of the :program:`mplpub` plotter.

  .. image:: ../examples/example_basic.png
     :width: 400
        
  .. container:: toggle

    .. container:: header

       Source code:

    .. literalinclude:: ../examples/example_basic.py
  
* A plot with different x scales (and labels) at the top and bottom.

  .. image:: ../examples/example_multiple_xscales.png
     :width: 400

  .. container:: toggle

    .. container:: header

       Source code:

    .. literalinclude:: ../examples/example_multiple_xscales.py

* A schematic plot with no ticks or quantitative data.

  .. image:: ../examples/example_schematic.png
     :width: 400

  .. container:: toggle

    .. container:: header

       Source code:

    .. literalinclude:: ../examples/example_schematic.py
             
* A fancy schematic figure that includes png insets (from OVITO in this case).

  .. image:: ../examples/example_fancy_schematic_embedded_png.png
     :width: 400

  .. container:: toggle

    .. container:: header

       Source code:
  
    .. literalinclude:: ../examples/example_fancy_schematic_embedded_png.py
