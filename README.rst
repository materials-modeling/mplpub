mplpub
======
`mplpub` is a simple python module that provides a number of templates for publication quality `matplotlib` figures.
It can be installed using `pip` as follows::

  python3 -m pip mplpub

More information can be found at https://mplpub.materialsmodeling.org.
