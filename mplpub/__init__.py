from mplpub.mplpub import setup
from mplpub.mplpub import tableau

__project__ = 'mplpub'
__description__ = 'Pretty plots with matplotlib and LaTeX',
__authors__ = ['Joakim Loefgren',
               'Anders Lindman',
               'Paul Erhart']
__copyright__ = '2023'
__license__ = 'MIT License'
__version__ = '1.1'
__maintainer__ = 'The mplpub developers team'
__maintainer_email__ = 'joakim.lofgren@aalto.fi'
__status__ = 'Stable'
__url__ = 'http://mplpub.materialsmodeling.org/'
__all__ = [
    'setup',
    'tableau',
]
