#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
from matplotlib._png import read_png
import mplpub

# Set figure specs
mplpub.setup(template='natcom', height=2.0)
fig = plt.figure()
gs = gridspec.GridSpec(1, 100)
ax1 = plt.subplot(gs[0, 2:24])
ax2 = plt.subplot(gs[0, 31:75])
ax3 = plt.subplot(gs[0, 81:100])
fig.subplots_adjust(left=0.035, bottom=0.15, right=1.01, top=0.98)

###################################################################
# sub figure 1
###################################################################

# Data to plot
mue = np.linspace(0, 3, 31)
s = 0.8 + 2 * mue
d = -0.5 + 2 * mue
ctl_s = 26
ctl_d = 15
s[ctl_s + 1:] = s[ctl_s]
d[ctl_d + 1:] = d[ctl_d]
be = 0.4

# plot settings
xmin = -0
xmax = 3
ymin = 0
ymax = 7

# plot data
ax1.plot(mue, d, '-', color=mplpub.tableau['turquoise'])
ax1.plot(mue, s, '-', color=mplpub.tableau['red'])
ax1.plot(
    mue[ctl_d],
    d[ctl_d],
    'o',
    markersize=3,
    markerfacecolor=mplpub.tableau['turquoise'],
    markeredgecolor=mplpub.tableau['turquoise'],
)
ax1.plot(
    mue[ctl_s],
    s[ctl_s],
    'o',
    markersize=3,
    markerfacecolor=mplpub.tableau['red'],
    markeredgecolor=mplpub.tableau['red'],
)

# grey shaded areas
ax1.fill_betweenx(
    [ymin, ymax],
    xmin,
    xmin + be,
    edgecolor=mplpub.tableau['lightgrey'],
    facecolor=mplpub.tableau['lightgrey'],
)
ax1.fill_betweenx(
    [ymin, ymax],
    xmax - be,
    xmax,
    edgecolor=mplpub.tableau['lightgrey'],
    facecolor=mplpub.tableau['lightgrey'],
)

# set x- and y-limits
ax1.set_xlim([xmin, xmax])
ax1.set_ylim([ymin, ymax])

# remove x- and y-ticks
ax1.set_xticks([])
ax1.set_yticks([])

# set axis labels
ax1.set_xlabel('Electron chemical\n potential')
ax1.set_ylabel('Formation energy')

# text pieces. the transform argument yields fractional coordinates
# for the text positions, which is very convenient.
ax1.text(0.17, 0.94, 'VB', transform=ax1.transAxes, fontsize=7)
ax1.text(0.84, 0.94, 'CB', transform=ax1.transAxes, fontsize=7, ha='right')
ax1.text(
    0.40,
    0.72,
    'shallow',
    transform=ax1.transAxes,
    fontsize=7,
    rotation=63,
    color=mplpub.tableau['red'],
)
ax1.text(
    0.27,
    0.20,
    'deep',
    transform=ax1.transAxes,
    fontsize=7,
    rotation=63,
    color=mplpub.tableau['turquoise'],
)
ax1.text(0.53, 0.30, 'CTL', transform=ax1.transAxes, fontsize=7)
ax1.text(
    0.03,
    0.94,
    r'\textbf{a}',
    transform=ax1.transAxes,
    bbox={'facecolor': 'white', 'alpha': 1, 'pad': 1.5, 'linewidth': 1},
)

###################################################################
# sub figure 2
###################################################################

# Data to plot
y_d_n_x0 = 2.5
y_d_n_y0 = 0.5
y_d_c_x0 = 4.3
y_d_c_y0 = 3.5
nbr_gridpoints = 701
x_range = np.linspace(0, 7, nbr_gridpoints)
y_d_n = y_d_n_y0 + 0.15 * (x_range - y_d_n_x0) ** 2
y_d_c = y_d_c_y0 + 0.80 * (x_range - y_d_c_x0) ** 2
y_s_c = y_d_c_y0 + 0.80 * (x_range - y_d_n_x0) ** 2

# plot settings
xmin = 0
xmax = 7
ymin = 0
ymax = 7

# plot data
ax2.plot(x_range[60:441], y_s_c[60:441], '-', color=mplpub.tableau['red'])
ax2.plot(x_range[0:571], y_d_n[0:571], '-k')
ax2.plot(x_range[240:621], y_d_c[240:621], '-', color=mplpub.tableau['turquoise'])

# set x- and y-limits
ax2.set_xlim([xmin, xmax])
ax2.set_ylim([ymin, ymax])

# remove x- and y-ticks
ax2.set_xticks([])
ax2.set_yticks([])

# set axis labels
ax2.set_xlabel('Configuration coordinate')
ax2.set_ylabel('Formation energy')

# remove borders
ax2.spines['right'].set_visible(False)
ax2.spines['top'].set_visible(False)

# text pieces. the transform argument yields fractional coordinates
# for the text positions, which is very convenient.
ax2.text(0.48, 0.02, 'neutral', transform=ax2.transAxes, fontsize=7, ha='center')
ax2.text(0.48, 0.95, 'charged', transform=ax2.transAxes, fontsize=7, ha='center')
ax2.text(
    0.09,
    0.93,
    'shallow',
    transform=ax2.transAxes,
    fontsize=7,
    color=mplpub.tableau['red'],
)
ax2.text(
    0.81,
    0.93,
    'deep',
    transform=ax2.transAxes,
    fontsize=7,
    color=mplpub.tableau['turquoise'],
)
ax2.text(
    0.015,
    0.94,
    r'\textbf{b}',
    transform=ax2.transAxes,
    bbox={'facecolor': 'white', 'alpha': 1, 'pad': 1.5, 'linewidth': 1},
)

# arrows
arrow_head_width = 0.08
arrow_head_length = 0.15
ax2.arrow(
    y_d_c_x0,
    y_d_c_y0,
    0,
    y_d_n[431] - y_d_c_y0,
    head_width=arrow_head_width,
    head_length=arrow_head_length,
    length_includes_head=True,
    fc='k',
    ec='k',
    zorder=20,
    linewidth=0.5,
)
ax2.arrow(
    y_d_n_x0,
    y_d_n_y0,
    0,
    y_d_c[251] - y_d_n_y0,
    head_width=arrow_head_width,
    head_length=arrow_head_length,
    length_includes_head=True,
    fc='k',
    ec='k',
    zorder=20,
    linewidth=0.5,
)

# embedd png images
arr_neutral = read_png('example_fancy_schematic_embedded_png_fig1.png')
imagebox = OffsetImage(arr_neutral, zoom=0.105)
ab = AnnotationBbox(imagebox, [0.9, 3.2], frameon=False)
ax2.add_artist(ab)
arr_neutral = read_png('example_fancy_schematic_embedded_png_fig2.png')
imagebox = OffsetImage(arr_neutral, zoom=0.105)
ab = AnnotationBbox(imagebox, [6.0, 3.2], frameon=False)
ax2.add_artist(ab)

###################################################################
# sub figure 3
###################################################################

# Data to plot
ctl_d = [0.75, 1.35]
ctl_s = [1.65, 2.25]
xstart = 0.5
xend = 2.5
nbr_gridpoints = 1001
x_range = np.linspace(xstart, xend, nbr_gridpoints)
y_min_range = 0.5 - 0.1 * np.sin(np.pi / 4.0 + 2 * np.pi * x_range / (xstart - xend))
y_max_range = 5.5 + 0.1 * np.sin(np.pi / 4.0 + 2 * np.pi * x_range / (xstart - xend))

# plot settings
xmin = 0.25
xmax = 3.0
ymin = 0.2
ymax = 6.2

# plot valence (blue) and conduction (orange) bands as filled regions
ax3.fill_between(
    x_range,
    y_min_range,
    2.0,
    edgecolor=mplpub.tableau['blue'],
    facecolor=mplpub.tableau['lightblue'],
    linewidth=0.5,
)
ax3.fill_between(
    x_range,
    4.0,
    y_max_range,
    edgecolor=mplpub.tableau['orange'],
    facecolor=mplpub.tableau['lightorange'],
    linewidth=0.5,
)

# plot data
ax3.plot(ctl_d, [3.0, 3.0], '-', color=mplpub.tableau['turquoise'], linewidth=1.5)
ax3.plot(ctl_s, [4.4, 4.4], '-', color=mplpub.tableau['red'], linewidth=1.5)

# set x- and y-limits
ax3.set_xlim([xmin, xmax])
ax3.set_ylim([ymin, ymax])

# remove x- and y-ticks
ax3.set_xticks([])
ax3.set_yticks([])

# set axis labels
ax3.set_ylabel('Quasi-particle energy')

# remove borders
ax3.spines['right'].set_visible(False)
ax3.spines['top'].set_visible(False)
ax3.spines['bottom'].set_visible(False)

# arrows (electrons in defect levels)
arrow_head_width = 0.08
arrow_head_length = 0.05
ax3.arrow(
    0.95,
    2.85,
    0,
    0.3,
    head_width=arrow_head_width,
    head_length=arrow_head_length,
    length_includes_head=True,
    fc='k',
    ec='k',
    zorder=20,
    linewidth=0.5,
    shape='right',
)
ax3.arrow(
    1.15,
    3.15,
    0,
    -0.3,
    head_width=arrow_head_width,
    head_length=arrow_head_length,
    length_includes_head=True,
    fc='k',
    ec='k',
    zorder=20,
    linewidth=0.5,
    shape='right',
)
ax3.arrow(
    1.85,
    3.85,
    0,
    0.3,
    head_width=arrow_head_width,
    head_length=arrow_head_length,
    length_includes_head=True,
    fc='k',
    ec='k',
    zorder=20,
    linewidth=0.5,
    shape='right',
)
ax3.arrow(
    2.05,
    4.15,
    0,
    -0.3,
    head_width=arrow_head_width,
    head_length=arrow_head_length,
    length_includes_head=True,
    fc='k',
    ec='k',
    zorder=20,
    linewidth=0.5,
    shape='right',
)

# text pieces. the transform argument yields fractional coordinates
# for the text positions, which is very convenient.
ax3.text(
    0.380,
    0.54,
    'shallow',
    transform=ax3.transAxes,
    fontsize=7,
    color=mplpub.tableau['red'],
)
ax3.text(
    0.120,
    0.38,
    'deep',
    transform=ax3.transAxes,
    fontsize=7,
    color=mplpub.tableau['turquoise'],
)
ax3.text(
    0.035,
    0.94,
    r'\textbf{c}',
    transform=ax3.transAxes,
    bbox={'facecolor': 'white', 'alpha': 1, 'pad': 1.5, 'linewidth': 1},
)

###################################################################
# save figure to file
###################################################################

fig.savefig('example_fancy_schematic_embedded_png.pdf')
