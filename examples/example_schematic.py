import numpy as np
import matplotlib.pyplot as plt
import mplpub

# Schematic illustration without ticks or other quantitative data
# Contributed by Erik Jedvik

mplpub.setup()

x = np.arange(0.0, 1.2, 0.01)
band = 5*(x - 0.4)**2 + 0.4
pol = 5*(x - 0.7)**2 + 0.2

fig, ax = plt.subplots()

ax.plot(x, band)
ax.plot(x, pol, color=mplpub.tableau['red'])
ax.plot([0.4, 0.7], [0.2, 0.2], '--k')
ax.plot([0.4, 0.7], [0.4, 0.4], '--k')
ax.plot([0.4, 0.7], [0.85, 0.85], '--k')

ax.set_xlabel('Generalised coordinate\n(lattice distortion)')
ax.set_ylabel('Energy (arb. units)')
ax.set_xlim([0.0, 1.1])
ax.set_ylim([0.0, 1.0])

# remove ticks and tick labels, the tick_params method will
# provide you with the most control
ax.tick_params(axis='both',  # which axex to operate on
               which='both',  # operate on both major and minor ticks
               bottom=False,  # turn bottom ticks off
               top=False,
               left=False,
               right=False,
               labelbottom=False,  # turns bottom ticklabels off
               labelleft=False)
# an quicker way with little room for customization
# ax.set_xticks([])
# ax.set_yticks([])

# now we annotate the schematic figure with some helpful text
ax.text(0.56, 0.75, 'band', rotation=55, color=mplpub.tableau['blue'])
ax.text(0.89, 0.70, 'polaron', rotation=55, color=mplpub.tableau['red'])
ax.text(0.29, 0.28, r'$E_\text{pol}$')
ax.text(0.32, 0.6, r'$E_\text{st}$')
ax.text(0.72, 0.6, r'$E_\text{el}$')
ax.annotate('', xy=(0.4, 0.2), xycoords='data',
            xytext=(0.4, 0.4), textcoords='data',
            size=12, va='center', ha='center',
            arrowprops=dict(arrowstyle='<|-|>',
                            connectionstyle='arc3, rad=0',
                            fc='k')
            )
ax.annotate('', xy=(0.4, 0.4), xycoords='data',
            xytext=(0.4, 0.85), textcoords='data',
            size=12, va='center', ha='center',
            arrowprops=dict(arrowstyle='<|-|>',
                            connectionstyle='arc3, rad=0',
                            fc='k')
            )
ax.annotate('', xy=(0.7, 0.2), xycoords='data',
            xytext=(0.7, 0.85), textcoords='data',
            size=12, va='center', ha='center',
            arrowprops=dict(arrowstyle='<|-|>',
                            connectionstyle='arc3, rad=0',
                            fc='k')
            )

plt.subplots_adjust(left=0.21, bottom=0.24,
                    right=0.97, top=0.95,
                    wspace=0.15, hspace=0.10)
plt.savefig('example_schematic.pdf')
