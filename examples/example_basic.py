import numpy as np
import matplotlib.pyplot as plt
import mplpub

# setup for publication quality plotting using default options,
# plot labels etc. will be rendedred by system LaTeX using a Helvetica
# sans-serif font
mplpub.setup(height=2.1, width=3.6)

# plot something
x = np.linspace(0, 1, 10)
y1 = np.exp(-x**2/2.0)
y2 = np.exp(-(x-2)**2/4.0)

fig, ax = plt.subplots()
ax.plot(x, y1, marker='s', label='Data 1')
ax.plot(x, y2, marker='o', label='Data 2')

# generally for latex we need an r before the string to not
# interpret backslash as an escape sequence
# also note that mplpub automatically loads amsmath so we can
# use the \text command
ax.set_xlabel(r'$x \,\text{(\normalfont\AA)}$')

# Glyphs for upper case greek letters are typically not included in sans fonts
# so to get them the mplpub module employs a dirty hack where we temporarily
# fall back to a serif font, this is handled automatically
# by the custom UG command:
ax.set_ylabel(r'$\UG{\Delta}E \,(\text{eV})$')

# the module comes with nice colors saved in dictionaries, the default
# colors are taken from the tableau palette but can also be accessed
# through a dictionary
ax.text(0.2, 0.7, r'$\sum_i\gamma_i\mathrm{d}A$',
        bbox={'facecolor': mplpub.tableau['lightorange'], 'alpha': 0.5, 'pad': 8})

ax.set_title(r'Sans text and math (except for uppercase greek)')
ax.legend()  # legends are automatically located at the "best" position

plt.tight_layout()
plt.savefig('example_basic.png')
