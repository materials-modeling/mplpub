import numpy as np
import matplotlib.pyplot as plt
import mplpub

###################################################################
# Data
###################################################################
# BaCeO3 data - Bassano et. al., Solid State Ionics 180, 168 (2009):
# fig 8, 1250 C-data
T_inv_bco = np.array([1.75, 2.1, 2.35, 2.675])  # 1000/T in Kelvin
T_bco = 1000.0 / T_inv_bco  # T in Kelvin
log_s_b_bco = np.array([-3.1, -3.8, -4.3, -5.0])  # log(sigma_bulk)
log_s_gb_bco = np.array([-4.9, -6.6, -7.6, -8.7])  # log(sigma_gb)
log_sT_b_bco = np.log10(10**(log_s_b_bco) * T_bco)  # log(sigma_bulk * T)
log_sT_gb_bco = np.log10(10**(log_s_gb_bco) * T_bco)  # log(sigma_gb * T)

# BaZrO3 data - Shirpour et. al., PCCP 14, 730 (2012): Fig 9, 6YBZ-data
T_bzo = 273 + np.array([150, 200, 250, 300, 350, 400,
                        450, 500, 550, 600, 700])  # T in Kelvin
T_inv_bzo = 1000.0 / T_bzo  # 1000/T in Kelvin
log_sT_b_bzo = np.array([-2.5, -1.9, -1.4, -1.0, -0.6, -0.4,
                         -0.3, 0.0, 0.2, 0.3, 0.5])  # log(sigma_bulk * T)
log_sT_gb_bzo = np.array([-9.3, -8.3, -7.1, -6.2, -5.5, -4.9,
                          -4.5, -4.1, -3.7, -3.2, -2.7])  # log(sigma_gb * T)

# Fits to data points
b_fit_bco = np.polyfit(T_inv_bco, log_sT_b_bco, 1)
gb_fit_bco = np.polyfit(T_inv_bco, log_sT_gb_bco, 1)
b_fit_bzo = np.polyfit(T_inv_bzo, log_sT_b_bzo, 1)
gb_fit_bzo = np.polyfit(T_inv_bzo, log_sT_gb_bzo, 1)

temp_inv = np.linspace(1, 2.8, 181)
line_b_bco = b_fit_bco[0] * temp_inv + b_fit_bco[1]
line_gb_bco = gb_fit_bco[0] * temp_inv + gb_fit_bco[1]
line_b_bzo = b_fit_bzo[0] * temp_inv + b_fit_bzo[1]
line_gb_bzo = gb_fit_bzo[0] * temp_inv + gb_fit_bzo[1]

###################################################################
# Plot settings
###################################################################
# Setup for publication quality plotting using ACS (American Chemical
# Society) template with a non-default option for figure height
mplpub.setup(template='acs', height=2.4)

# X and Y label strings
x_label_string1 = r'$1000/T$ ($\text{K}^{-1}$)'
x_label_string2 = r'$T$ (K)'
y_label_string = r'$\log{(\sigma T / \text{S}\,\text{cm}^{-1}\text{K})}$'

# X and Y tick postions
xmin = 1.0
xmax = 2.8
dx = 0.3
ddx = 0.15
nx = int(round((xmax - xmin) / dx) + 1)
x_tick_values1 = xmin + dx * np.array(range(nx))

x_tick_values2 = np.array([400, 500, 700, 1000])
x_tick_positions2 = 1000.0 / x_tick_values2

ymin = -12
ymax = 2
dy = 2
ddy = 0
ny = int(round((ymax - ymin) / dy) + 1)
y_tick_values = ymin + dy * np.array(range(ny))

###################################################################
# Plot figure
###################################################################
fig, ax1 = plt.subplots()
ax1.plot(temp_inv, line_b_bco, '-', color=mplpub.tableau['blue'])
ax1.plot(temp_inv, line_gb_bco, '--', color=mplpub.tableau['blue'])
ax1.plot(temp_inv, line_b_bzo, '-', color=mplpub.tableau['orange'])
ax1.plot(temp_inv, line_gb_bzo, '--', color=mplpub.tableau['orange'])
ax1.plot(
    T_inv_bco,
    log_sT_b_bco,
    'o',
    markeredgecolor=mplpub.tableau['blue'],
    markerfacecolor=mplpub.tableau['lightblue'],
    label=r'BCO bulk')
ax1.plot(
    T_inv_bco,
    log_sT_gb_bco,
    's',
    markeredgecolor=mplpub.tableau['blue'],
    markerfacecolor=mplpub.tableau['lightblue'],
    label=r'BCO GB')
ax1.plot(
    T_inv_bzo,
    log_sT_b_bzo,
    'o',
    markeredgecolor=mplpub.tableau['orange'],
    markerfacecolor=mplpub.tableau['lightorange'],
    label=r'BZO bulk')
ax1.plot(
    T_inv_bzo,
    log_sT_gb_bzo,
    's',
    markeredgecolor=mplpub.tableau['orange'],
    markerfacecolor=mplpub.tableau['lightorange'],
    label=r'BZO GB')

# Sets the x- and y-scales
ax1.set_xlim([xmin - ddx, xmax + ddx])
ax1.set_ylim([ymin - ddy, ymax + ddy])
ax1.set_xlabel(x_label_string1)
ax1.set_ylabel(y_label_string)
ax1.set_xticks(x_tick_values1)
ax1.set_yticks(y_tick_values)

# Sets the top x-scale
ax2 = ax1.twiny()
ax1.xaxis.set_ticks_position('bottom')
ax2.xaxis.set_ticks_position('top')
ax2.axes.set_xticks(x_tick_positions2)
ax2.axes.set_xticklabels(x_tick_values2, position=(0, 0.98))
ax2.set_xlim([xmin - ddx, xmax + ddx])
ax2.set_xlabel(x_label_string2, labelpad=6.4)

ax1.legend()  # legends are automatically located at the "best" position

ax1.text(2.20, -01.0, r'$-$0.45 eV', fontsize=8)
ax1.text(2.34, -04.3, r'$-$0.46 eV', fontsize=8)
ax1.text(2.09, -06.5, r'$-$0.79 eV', fontsize=8)
ax1.text(1.88, -10.0, r'$-$0.99 eV', fontsize=8)

# Adjust figure to remove white space at edges. Can be used even without
# subplots.
plt.subplots_adjust(left=0.175, bottom=0.17, right=0.97, top=0.85)

plt.savefig('example_multiple_xscales.pdf')
